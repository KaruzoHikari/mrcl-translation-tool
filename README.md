# Kaz's Translation Tool
In order to use the tool, you need to launch `KazTranslationTool.exe` with the following flags:
* `-url`: The URL of the "export to CSV" Google Sheet (minus the GID)
* `-gids`: The list of sheet GIDs (shown at the end of the URL), separated by spaces
* `-aliases`: The name of the header row, and the name of the files it should be exported to.
  <br>Example: ```"en_en:en_en" "es_es:es_mx,es_es,es_ve" "fr_fr:fr_fr,fr_ca"```
* `-output`: The output path for the .json files. Defaults to the parent folder. 
* `-headrows`: The nº of header rows in the sheet. Defaults to 1
* `-ignoregit`: Disable the commit + push to git, if available