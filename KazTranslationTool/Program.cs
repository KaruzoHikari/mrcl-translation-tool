﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Collections.ObjectModel;
using System.IO;
using System.Linq;
using System.Management.Automation;
using System.Net;
using CsvHelper;
using Newtonsoft.Json;

namespace KazTranslationTool
{
    
    internal class Program
    {

        private static List<string> paths = new List<string>();
        private static Dictionary<string, List<Term>> langDictionary = new Dictionary<string, List<Term>>();

        private static string url;
        private static string outputPath;
        private static Dictionary<string, string[]> fileAliases = new Dictionary<string, string[]>();
        private static List<string> gids = new List<string>();
        private static bool pushToGit = true;
        private static int headerRows = 1;
        
        public static void Main(string[] args)
        {
            FindFlags(args);
            if (string.IsNullOrEmpty(outputPath))
            {
                outputPath = Directory.GetParent(Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().Location)).FullName;
            }
            Console.WriteLine("*************************************\n" +
                              "  WELCOME TO KAZ'S TRANSLATION TOOL  \n" +
                              "*************************************\n");
            Console.Write("Downloading translations...\n");
            UpdateTranslations();
            Console.ReadLine();
        }

        private static void FindFlags(string[] args)
        {
            string flag = null;
            List<string> flagArgs = new List<string>();

            foreach (string arg in args)
            {
                if (arg.StartsWith("-"))
                {
                    if (!string.IsNullOrEmpty(flag))
                    {
                        HandleFlag(flag, flagArgs);
                    }

                    flag = arg;
                    flagArgs.Clear();
                }
                else
                {
                    flagArgs.Add(arg);
                }
            }
            HandleFlag(flag,flagArgs);
        }

        private static void HandleFlag(string flag, List<string> args)
        {
            switch (flag)
            {
                case "-url":
                {
                    url = args[0];
                    break;
                }
                case "-gids":
                {
                    gids.AddRange(args);
                    break;
                }
                case "-output":
                {
                    outputPath = args[0];
                    break;
                }
                case "-aliases":
                {
                    foreach (string arg in args)
                    {
                        string lang = arg.Split(':')[0];
                        string[] aliases = arg.Split(':')[1].Split(',');
                        fileAliases.Add(lang,aliases);
                    }
                    break;
                }
                case "-ignoregit":
                {
                    pushToGit = false;
                    break;
                }
                case "-headrows":
                {
                    headerRows = Int32.Parse(args[0]);
                    break;
                }
            }
        }

        private static void UpdateTranslations()
        {
            DownloadTranslations();
            Console.Write("\nDownloaded! Exporting translations...\n");
            ExportTranslations();
            Console.Write("\nExported!");
            if (pushToGit)
            {
                Console.WriteLine("\n\nPushing to Git (if available...)");
                TryToPush();
            }
            Console.WriteLine("\nDone!\n");
        }

        private static void DownloadTranslations()
        {
            foreach (string gid in gids)
            {
                Console.WriteLine("Downloading from GID=" + gid + "...");
                string downloadLink = url + gid;
                WebClient client = new WebClient();

                string path = Path.GetTempFileName();
                paths.Add(path);
                client.DownloadFile(downloadLink, path);
            }
        }
        
        private static void ExportTranslations()
        {
            foreach (string path in paths)
            {
                using (var reader = new StreamReader(path))
                using (var csv = new CsvReader(reader, CultureInfo.InvariantCulture))
                {
                    csv.Read();
                    csv.ReadHeader();
                    
                    string[] headerRow = csv.HeaderRecord;
                    foreach (string lang in headerRow)
                    {
                        if (lang != "ID" && !langDictionary.ContainsKey(lang))
                        {
                            langDictionary[lang] = new List<Term>();
                        }
                    }

                    int skippedRows = 1;
                    while (csv.Read())
                    {
                        if (skippedRows < headerRows)
                        {
                            skippedRows++;
                            continue;
                        }
                        string id = csv.GetField("ID");
                        foreach (string lang in langDictionary.Keys)
                        {
                            string value = csv.GetField(lang);
                            langDictionary[lang].Add(new Term(id,value));
                        }
                    }
                }
            }

            PurgeListCategories();

            foreach (string lang in fileAliases.Keys)
            {
                WriteListFiles(lang,fileAliases[lang]);
            }
        }

        private static void WriteListFiles(string lang, string[] targets)
        {
            List<Term> termList = langDictionary[lang];
            Dictionary<string, string> localization = new Dictionary<string, string>();
            foreach (Term term in termList)
            {
                if (!string.IsNullOrEmpty(term.translation))
                {
                    try
                    {
                        localization.Add(term.id, term.translation);
                    }
                    catch (Exception e)
                    {
                        // Ignored (prob already added)
                    }
                }
            }

            string json = JsonConvert.SerializeObject(localization, Formatting.Indented);
            foreach (string target in targets)
            {
                File.WriteAllText(Path.Combine(outputPath,target + ".json"), json);
            }
        }

        private static void PurgeListCategories()
        {
            foreach (string lang in langDictionary.Keys)
            {
                List<Term> termList = langDictionary[lang];
                foreach(Term term in new List<Term>(termList))
                {
                    if (term.id.ToLower().Contains("[category]"))
                    {
                        termList.Remove(term);
                    }
                }
            }
        }

        private static void TryToPush()
        {
            string directory = GetCommonGitPath(); // directory of the git repository
            if (Directory.Exists(Path.Combine(directory, ".git")))
            {
                Console.WriteLine("\nFound Git repo, pushing...");
                using (PowerShell powershell = PowerShell.Create())
                {
                    powershell.AddScript("cd " + directory);
                    powershell.AddScript(@"git init");
                    powershell.AddScript(@"git pull");
                    powershell.AddScript(@"git add assets\minecraft\lang\*");
                    powershell.AddScript(@"git commit -m 'Updated Localization Files'");
                    powershell.AddScript(@"git push");

                    Collection<PSObject> results = powershell.Invoke();
                }
            }
            else
            {
                Console.WriteLine("Couldn't find Git repo.");
            }
        }

        private static String GetCommonGitPath()
        {
            return Path.GetFullPath(Path.Combine(System.Reflection.Assembly.GetExecutingAssembly().Location, "..", "..", "..", "..", ".."));
        } 
    }

    class Term
    {
        public string id { get; set; }
        public string translation { get; set; }

        public Term(string id, string translation)
        {
            this.id = id;
            this.translation = translation;
        }
    }
}